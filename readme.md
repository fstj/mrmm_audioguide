# MRMM audioguide

An audioguide for the collection of the Maison de la Résistance Mathias Mathieu.

## Overview

This audioguide is built using the Jekyll audioguide template provided by the Australian Centre for the Moving Image (ACMI).

Many thanks to the ACMI for sharing their audioguide template as free software.

Refer to this template and its documentation for more information about the code: https://github.com/ACMILabs/static-museum-audio-guide

## Getting Started

To locally visualize the audioguide, simply run `jekyll serve` and open the URL `http://127.0.0.1:4000` in a web browser.

To avoid requiring a web server when using the audioguide locally, you can convert the audioguide using wget, with the following command, after running `jekyll serve`:
```
wget -p -k -K -r -E --restrict-file-names=windows http://127.0.0.1:4000
```

## Modifications and reused code snippets

This site includes `markdown.rb` in the _plugins folder, by Tomotaka Sakuma. The gist on Github is located here: https://gist.github.com/tmtk75/1408402. It was refered in this post: http://wolfslittlestore.be/2013/10/rendering-markdown-in-jekyll/

The above snippet allows you to easily use markdown includes in the site, however it breaks support for Github pages, so we've removed useage from this repo. In the index.html page for example, you could use `{% markdown overview.md %}` to include the markdown text in `overview.md` without having to write HTML. The on boarding pages could also be refactored to use this.

When the site is added to the homescreen on iOS, a code snippet from [iosweblinks](https://github.com/stylr/iosweblinks) (original author [Kyle Barrow](https://gist.github.com/kylebarrow/1042026)) is used to get links to remain in the web app view rather than opening up Safari.

To check whether or not to make the player fixed position to the bottom of the screen, the code borrows the `$.support.fixedPosition` check from jQuery mobile, which doesn't do a true check of the feature, but eliminates known browsers that have issues with that CSS property. This enables fallback to an in-flow position for the player, which makes it sit at the very bottom of the page. We've placed a minimal version of the check in the head of the page, but the full jQuery mobile snippet is here: https://github.com/jquery/jquery-mobile/blob/master/js/support.js

## Included JS libraries

Most of the web app (aside from audio!) works without JavaScript, and use of JS libraries is kept to a progressive enhancement approach as much as possible. The following JS libraries are included here, each is covered by their own MIT license also in the repo. If you're extending this project, you may wish to switch to CDN copies, or concatenate and minify your libraries.

1. [jQuery (v1.12.3)](https://jquery.com/)
2. [Slick JS (v1.6.0)](http://kenwheeler.github.io/slick/)
3. [jPlayer (v2.9.2)](http://jplayer.org/)

In this repo, we've chosen to keep these libraries as local assets so that running `jekyll serve` or `jekyll build` from the command-line is as simple as possible. However, you may wish to switch to loading these libraries from a [package library](https://www.npmjs.com/) instead.

## License

The MRMM audioguide code is released under the terms of the MIT license. The content of the audioguide itself, which is the text of the audioguide (stored in the directory `stops`), the audio files (stored in the directory `audio`), and the images displayed in the audioguide (stored in the directory `assets/img/stops`), is distributed under the Creative Commons Attribution-ShareAlike 3.0 Unported License ([CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0)).

Included JS libraries are covered by their own licenses, included in this repo.
