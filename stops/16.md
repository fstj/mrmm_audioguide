---
layout: episode
permalink: /stops/16/
type: stop
section_title: La Résistance
title:
page_rank: 16
stop_id: 16
audio_file: 16_la_resistance.mp3
hero_images:
hero_images:
 - path: 16-01-croix_de_lorraine.jpg
   alt_text: Croix de Lorraine sur une porte
---

La Résistance regroupe l'ensemble des mouvements et réseaux clandestins qui lors de la Seconde Guerre Mondiale ont lutté contre les forces allemandes et l’état français collaborationniste.

Le 18 juin 1940, alors que Pétain appelle à cesser le combat, le général de Gaulle lance l’appel à la Résistance depuis Londres.

La Résistance va s’organiser autour de personnalités telles que le général de Gaulle, le général Delestraint, Jean Moulin.

Localement, ils ont pour nom Drouot, De Lassus Saint Geniès, Michel Prunet.

Le mouvement qui, peu à peu, s’amplifie va se structurer.

Des groupes, sans liens véritables au début, vont mener des opérations contre les troupes d’occupation et les forces mises en place par Vichy dans les villes et les campagnes.

> Au début de l’année 1943, le premier Maquis de France s’installe à la ferme d’Ambel à Omblèze près du col de la Bataille.

> Simultanément, Marcel Barbu, fondateur de l’usine Boimondau, les Boîtiers de Montres du Dauphiné, à Valence, achète la ferme Mourras à Combovin dans laquelle est créée une école de formation des cadres de la Résistance.

> Les nombreux réfractaires au STO qui se cachent dans les fermes de la région rejoignent alors le Maquis.

> Avec la complicité des habitants, ils préparent dans la clandestinité des opérations de sabotage comme le montre la scène dans la cuisine de ferme reconstituée.
