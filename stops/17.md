---
layout: episode
permalink: /stops/17/
type: stop
section_title: Sabotage
title:
page_rank: 17
stop_id: 17
audio_file: 17_sabotage.mp3
hero_images:
 - path: 17-01-manuel_sabotage.jpg
   alt_text: Extrait d'un manuel de sabotage
 - path: 17-02-manuel_sabotage.jpg
   alt_text: Extrait d'un manuel de sabotage
---

> Le sabotage est une action volontaire de destruction des biens matériels de l’ennemi, menée dans la clandestinité par un groupe ou une organisation.

> Dans la vallée, Louis Pinat, futur membre de la Compagnie Morin participera en mars 1944 au sabotage de la ligne téléphonique du Dérot, près de Beaufort, afin d’interdire les communications.

> Un autre évènement aura un écho considérable dans la région.

> Dans la nuit du 21 au 22 décembre 1943, à Vercheny, le sabotage de la ligne ferroviaire Briançon-Livron cause la mort de 19 permissionnaires allemands.

> La répression est terrible : quelques jours après les allemands effectuent une rafle au cours de laquelle 57 personnes des villages alentours (Barsac, Pontaix, Sainte-Croix et Vercheny) seront faits prisonniers et déportés.

> 38 d’entre eux n’en reviendront pas.
