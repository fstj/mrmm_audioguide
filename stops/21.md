---
layout: episode
permalink: /stops/21/
type: stop
section_title: Le Vercors
title:
page_rank: 21
stop_id: 21
audio_file: 21_le_vercors.mp3
hero_images:
---

La compréhension des événements qui se sont déroulés dans et autour du Vercors nécessite de connaître le plan conçu en décembre 1942, par Pierre Dalloz, architecte urbaniste et pionnier de l’alpinisme.

Pour lui, le Vercors, terre d’accueil de réfugiés et de réfractaires au STO, dispose d’atouts stratégiques majeurs que lui confère sa situation géographique.

Ce plan dit « plan montagnard » dans sa version de décembre 1942 comporte deux étapes :

- Un « programme d'action immédiat » consistant en la création de Maquis, l'aménagement de terrains d’atterrissage clandestins, le stockage d'armes et d'explosifs et la défense des principaux points d'accès au plateau.

- Une seconde étape est envisagée dans le cadre d'une reconquête de la France par les Alliés.

  Le plateau pourrait alors recevoir des troupes aéroportées et être la base de départ de raids visant à désorganiser les voies de communications ennemies tout autour du plateau.

Les allemands mesurent, eux aussi, l’importance stratégique du Vercors et vont tenter de neutraliser les possibilités offertes par cette forteresse naturelle.

Les Maquis se positionnent autour du Vercors pour en interdire l’accès.

Les FFI de la Drôme prennent position sur les versants Ouest et Sud.

Pour les allemands il s’agit désormais de déloger ces Maquisards qui empêchent l’accès au Vercors.

Les opérations sont menées par les services de police français et les forces allemandes.

L'opération allemande est la plus vaste entreprise menée en France contre la Résistance.

> Les premières opérations de « nettoyage » du Vercors débutent le 16 avril 1944.

> La carte et le tableau permettent de connaître et situer les principaux événements qui se sont déroulés chronologiquement, dans et autour de la vallée de la Gervanne, entre février 1943 et août 1944.

> Ils permettent également de situer les différentes positions de la compagnie Morin de sa création, le 6 juin, à la libération de la Drôme, fin août 1944.
