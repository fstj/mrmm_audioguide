---
layout: episode
permalink: /stops/22/
type: stop
section_title: Le Lieutenant Prunet dit « Michel » (1920 – 1944)
title:
page_rank: 22
stop_id: 22
audio_file: 22_le_lieutenant_prunet_dit_michel.mp3
hero_images:
 - path: 22-01-groupe_resistants_michel_prunet.jpg
   alt_text: Groupe de résistants autour de Michel Prunet
---

Michel Prunet, diplômé de l’école des Sciences politiques, est un jeune avocat du barreau de Paris.

Très vite il entre en Résistance.

Il rejoint les proches de Drouot qui commandera les FFI de la Drôme.

Il devient d’abord l’adjoint de René Fanget, chef du bataillon « Drôme Nord » avant d’être désigné pour prendre le commandement d’un maquis en formation à Combovin.

Le 4 juin, il est arrêté par un détachement de police allemande à Saint Jean en Royans et emmené au fort de Montluc à Lyon.

Il est fusillé, avec 27 autres patriotes, le 12 juillet 1944 à Toussieu, dans l’Isère, lieu-dit « La Perrière ».
