---
layout: episode
permalink: /stops/31/
type: stop
section_title: L’Alsace
title:
page_rank: 31
stop_id: 31
audio_file: 31_l_alsace.mp3
hero_images:
 - path: 31-01-compagnie_morin_en_alsace.jpg
   alt_text: État-major de la Compagnie Morin en Alsace
---

Le 1er janvier 1945, la Compagnie devient la 10ème Compagnie du 3ème Bataillon du 159ème Régiment d’Infanterie Alpine dans le cadre de la récente création de la 27ème Division Alpine commandée par le colonel Vallette d’Osia.

Devant la menace d’une reprise de Strasbourg par les troupes allemandes en pleine offensive hivernale, le Général de Lattre de Tassigny demande le renfort de la 5ème Demi-Brigade de Chasseurs Alpins.

Cette demi-brigade est composée d’unités possédant parfaitement la maîtrise des techniques alpines de combat.

> Le Général de Lattre de Tassigny décide d’affecter le 159ème Régiment d’Infanterie Alpine en renfort de la 3ème Division d’Infanterie Algérienne dans le plan de défense de la capitale alsacienne.

> C’est au « 15-9 » que revient l'honneur de partir pour l'Alsace.

> La compagnie Morin, participe ainsi à la défense de Strasbourg.

> Elle perdra deux hommes lors de ce séjour qui, là aussi, se déroulera dans des conditions de vie difficiles.
