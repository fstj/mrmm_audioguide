---
layout: episode
permalink: /stops/32/
type: stop
section_title: La Maurienne (2ème séjour)
title:
page_rank: 32
stop_id: 32
audio_file: 32_la_maurienne_2eme_sejour.mp3
hero_images:
 - path: 32-01-compagnie_morin_mont_cenis.jpg
   alt_text: Passage du col du Mont-Cenis
 - path: 32-02-compagnie_morin_mont_cenis.jpg
   alt_text: Arrivée de la compagnie Morin en Italie
---

> Le 3 mars, la compagnie quitte les bords du Rhin.

> Après quelques jours de repos et d’entraînement au Touvet dans l’Isère, elle rejoint, le 8 avril, une nouvelle fois la Maurienne.

> Elle va y effectuer diverses missions de soutien et de combat.

> Souvent mal équipés, c’est dans leur mental que « les Alpins » puisent pour mener au mieux les opérations militaires sur des skis équipés de peaux de phoques, quand ils en ont.

> Combien de nuits dans des trous creusés dans la neige ?

> Mais ils savent que de leurs actions peut dépendre la vie de leurs camarades des Bataillons Alpins qui sont au contact de l’ennemi à quelques centaines de mètres.

> Le 8 mai est signé l’acte définitif de capitulation.

> C’est la fin officielle de la Seconde Guerre Mondiale en Europe.

> Les troupes françaises pénètrent en Italie, mais le 9 juin, le Général de Gaulle décide, sous la contrainte anglo-américaine, de ramener les troupes jusqu’à la frontière de 1939.

> La Compagnie se rend dès lors dans le Jura où elle prépare son départ pour l’Autriche.
