---
layout: episode
permalink: /stops/33/
type: stop
section_title: L’occupation en Allemagne
title:
page_rank: 33
stop_id: 33
audio_file: 33_l_occupation_en_allemagne.mp3
hero_images:
---

La compagnie n’ira pas en occupation en Allemagne hormis quelques éléments qui dans une démarche personnelle rejoindront les troupes de de Lattre.

On peut voir dans la vitrine des objets et des équipements de l’un de ces éléments.

La vitrine a surtout été constituée pour évoquer, à partir de pièces de collections, l’extermination de certaines populations.

La vallée de la Gervanne, si elle a caché quelques familles juives, n’a pas fait l’objet de rafles telles que celles qui ont été commises dans d’autres régions.

Mais nous avons voulu rappeler cet épisode tragique qu’a été la déportation d’hommes et de femmes qui parce que juifs ou tsiganes, homosexuels, communistes ont été la proie du système nazi instauré par Hitler.

Le document présenté est un programme d’une soirée de bienfaisance au cours de laquelle a été projeté le film « Au cœur de l’orage » dont le thème est la Résistance dans le Maquis du Vercors.

Berlin abritait le poste de commandement à partir duquel Hitler donnait la plupart de ses directives meurtrières qui mettront à feu et à sang le monde entier.

Les Briques proviennent de son bunker.

Il s’y donnera la mort le 30 avril 1945.
